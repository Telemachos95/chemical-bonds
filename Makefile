
isotopes: main.o atom.o down_quark.o up_quark.o gluon.o electron.o proton.o neutron.o nucleus.o
	g++ -o isotopes main.o atom.o down_quark.o up_quark.o gluon.o electron.o proton.o neutron.o nucleus.o -g
	
main.o: main.cpp atom.h
	g++ -o main.o -c main.cpp
	

atom.o: atom.cpp nucleus.h atom.h proton.h
	g++ -o atom.o -c atom.cpp
 
down_quark.o: down_quark.cpp down_quark.h
	g++ -o down_quark.o -c down_quark.cpp

up_quark.o: up_quark.cpp up_quark.h
	g++ -o up_quark.o -c up_quark.cpp

gluon.o: gluon.cpp gluon.h
	g++ -o gluon.o -c gluon.cpp

electron.o: electron.cpp electron.h
	g++ -o electron.o -c electron.cpp

proton.o: proton.cpp proton.h 
	g++ -o proton.o -c proton.cpp

neutron.o: neutron.cpp neutron.h
	g++ -o neutron.o -c neutron.cpp

nucleus.o: nucleus.cpp nucleus.h proton.h neutron.h
	g++ -o nucleus.o -c nucleus.cpp

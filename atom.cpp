#include <iostream>
#include "nucleus.h"
#include "atom.h"
#include "proton.h"
using namespace std;

atom::atom(int protons, int neutrons) {
    my_nucleus = new nucleus(protons, neutrons);
    cout << "An atom has been constructed" << endl << "protons = " << protons << ", neutrons = " << neutrons << endl;
}

atom::~atom() {
    delete my_nucleus;
    cout << "An atom will be destroyed" << endl << "atomic number = " << atomic_number << ", mass number = " << mass_number << endl << endl;
}

void atom::set_mass_number(int protons, int neutrons) {
    mass_number = protons + neutrons;
}

int atom::get_mass_number() {
    return mass_number;
}

void atom::set_atom_number(int protons) {
    atomic_number = protons;
}

int atom::get_atom_number() {
    return atomic_number;
};

void atom::set_electric_charge_electrons(int electrons) {
    electric_charge_electrons = electrons * (-1); // our atom is not ionized,protons == electrons,electric_charge_electrons = number of electrons * (-1e) , -1e is the electric charge of an electron. 
}

int atom::get_electric_charge_electrons() {
    return electric_charge_electrons;
}

void atom::set_electric_charge_protons(int protons) {
    electric_charge_protons = protons * 1; // electric_charge_protons = number of protons * 1e ,1e is the electric charge of a proton.
}

int atom::get_electric_charge_protons() {
    return electric_charge_protons;
}

void atom::set_electric_charge() {
    electric_charge = electric_charge_electrons + electric_charge_protons;
}

int atom::get_electric_charge() {
    return electric_charge;
};

void atom::print() {
    cout << "Atom: " << "atomic number = " << atomic_number << ", mass number = " << mass_number << ", electric charge = " << electric_charge << endl;
    my_nucleus->print();
}
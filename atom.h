#ifndef ATOM
#define ATOM

#include "nucleus.h"
#include "electron.h"

class atom {
private:
    nucleus* my_nucleus;
    electron electrons[6];
    int mass_number;
    int atomic_number;
    int electric_charge;
    int electric_charge_electrons;
    int electric_charge_protons;
public:
    atom(int protons, int neutrons);
    ~atom();
    void set_mass_number(int protons, int neutrons);
    int get_mass_number();
    void set_atom_number(int protons);
    int get_atom_number();
    void set_electric_charge_electrons(int electrons);
    int get_electric_charge_electrons();
    void set_electric_charge_protons(int protons);
    int get_electric_charge_protons();
    void set_electric_charge();
    int get_electric_charge();
    void print();
};

#endif

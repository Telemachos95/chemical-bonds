
#include <iostream>
#include "down_quark.h"
using namespace std;

down_quark::down_quark() {
    electric_charge = "-1/3e";
    mass = "4.7 MeV/c*c";
    spin = 0.5;
    statistics = "Fermi-Dirac";
    cout << "A down quark has been constructed" << endl;
}

down_quark::~down_quark() {
    cout << "An down quark will be destroyed" << endl;
}

void down_quark::print() {
    cout << "down quark: " << "mass = " << mass << " spin = " << spin << " electric charge = " << electric_charge << " statistics = " << statistics << endl;
}


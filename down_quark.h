#ifndef DOWN_QUARK
#define DOWN_QUARK

#include <string>
using namespace std;

class down_quark {
private:
    string electric_charge;
    string mass;
    float spin;
    string statistics;
public:
    down_quark();
    ~down_quark();
    void print();
};

#endif
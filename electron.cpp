
#include <iostream>
#include "electron.h"
using namespace std;



electron::electron() {
    electric_charge = "-1e";
    mass = "5.48579909070 × 10−4 u";
    spin = 0.5;
    statistics = "Fermi-Dirac";
    cout << "A electron has been constructed" << endl;
}

electron::~electron() {
    cout << "An electron will be destroyed" << endl;
}

void electron::print() {
    cout << "electron: " << "mass = " << mass << ", spin = " << spin << ", electric charge = " << electric_charge << ", statistics = " << statistics << endl;
}
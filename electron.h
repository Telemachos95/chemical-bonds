
#include <string>
using namespace std;

class electron {
private:
    string electric_charge;
    string mass;
    float spin;
    string statistics;
public:
    electron();
    ~electron();
    void print();
};
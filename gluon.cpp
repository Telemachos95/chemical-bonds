
#include <iostream>
#include "gluon.h"
using namespace std;

gluon::gluon() {
    electric_charge = "0e";
    mass = "0";
    spin = 1;
    statistics = "Bose-Einstein";
    cout << "A gluon has been constructed" << endl;
}

gluon::~gluon() {
    cout << "An gluon will be destroyed" << endl;
}

void gluon::print() {
    cout << "gluon: " << "mass = " << mass << " spin = " << spin << " electric charge = " << electric_charge << " statistics = " << statistics << endl;
}

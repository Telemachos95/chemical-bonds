
#ifndef GLUON
#define GLUON

#include <string>
using namespace std;

class gluon {
private:
    string electric_charge;
    string mass;
    float spin;
    string statistics;
public:
    gluon();
    ~gluon();
    void print();
};

#endif
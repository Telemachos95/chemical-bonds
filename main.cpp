
#include <iostream>
#include <cstdlib>
#include <time.h>
#include "atom.h"
using namespace std;

int main(int argc, char** argv) {

    atom** array;

    int rand_no_isotopes = rand() % (10 - 5 + 1) + 5; //Paragontai apo 5 ews 10 isotopa an8raka
    cout << rand_no_isotopes << " Random carbon isotopes will be created " << endl << endl;

    array = new atom*[rand_no_isotopes];
    int rand_type_protons = 6;
    int counter_protons = 0;
    int counter_neutrons = 0;
    int total_electric_charge = 0;
    srand (time(NULL));
    
    for (int i = 0; i < rand_no_isotopes; i++) {
        int rand_type_neutrons = rand() % (8 - 6 + 1) + 6;
        array[i] = new atom(rand_type_protons, rand_type_neutrons);
        cout << rand_type_neutrons << endl << endl << endl;
        counter_protons += rand_type_protons;
        counter_neutrons += rand_type_neutrons;
        array[i]->set_electric_charge_electrons(rand_type_protons); //protons == electrons
        array[i]->set_electric_charge_protons(rand_type_protons);
        array[i]->set_electric_charge();
        total_electric_charge += array[i]->get_electric_charge();
    }

    for (int i = 0; i < rand_no_isotopes; i++) {
        cout << endl << endl;
        array[i]->print();
        cout << endl << endl;
    }

    cout << endl << endl;
    cout << "Total Number of Protons: " << counter_protons << endl;
    cout << "Total Number of Neutrons: " << counter_neutrons << endl;
    cout << "Total Electric Charge Created: " << total_electric_charge << "e" << endl;
    cout << endl << endl;

    for (int i = 0; i < rand_no_isotopes; i++) {
        delete array[i];
	cout << "\n";
    }

    delete[] array;

    return 0;
}


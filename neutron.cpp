
#include <iostream>
#include "neutron.h"
using namespace std;

neutron::neutron() {
    mass = "1.0085 u";
    spin = 0.5;
    electric_charge = "0e";
    statistics = "Fermi-Dirac";
    cout << "A neutron has been constructed" << endl;
}

neutron::~neutron() {
    cout << "A neutron will be destroyed" << endl;
}

void neutron::print() {
    cout << "neutron: " << "mass = " << mass << " spin = " << spin << " electric charge = " << electric_charge << " statistics = " << statistics << endl;
    down_quark1.print();
    down_quark2.print();
    up_quark1.print();
    gluon1.print();
    gluon2.print();
    gluon3.print();
}
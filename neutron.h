
#ifndef NEUTRON
#define NEUTRON

#include <string>
#include "down_quark.h"
#include "up_quark.h"
#include "gluon.h"
using namespace std;

class neutron {
private:
    down_quark down_quark1;
    down_quark down_quark2;
    up_quark up_quark1;
    gluon gluon1;
    gluon gluon2;
    gluon gluon3;
    string mass;
    float spin;
    string electric_charge;
    string statistics;
public:
    neutron();
    ~neutron();
    void print();
};

#endif
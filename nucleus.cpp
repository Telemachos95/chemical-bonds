
#include "nucleus.h"
#include "proton.h"
#include "neutron.h"



nucleus::nucleus(int prot_number, int neut_number) {
    prot_no = prot_number;
    neut_no = neut_number;
    protons = new proton[prot_number];
    neutrons = new neutron[neut_number];
}

nucleus::~nucleus() {
    delete[] protons;
    delete[] neutrons;
}

void nucleus::print() {
    for (int i = 0; i < prot_no; i++) {
        protons[i].print();
    }
    for (int i = 0; i < neut_no; i++) {
        neutrons[i].print();
    }
}
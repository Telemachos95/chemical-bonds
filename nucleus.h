
#ifndef NUCLEUS
#define NUCLEUS

#include "proton.h"
#include "neutron.h"

class nucleus {
private:
    proton* protons;
    neutron* neutrons;
    int prot_no;
    int neut_no;
public:
    nucleus(int prot_number, int neut_number);
    ~nucleus();
    void print();
};

#endif

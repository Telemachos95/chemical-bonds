
#include <iostream>
#include <string>
#include "proton.h"
using namespace std;

proton::proton() {
    mass = "1.007 u";
    spin = 0.5;
    electric_charge = "+1e";
    statistics = "Fermi-Dirac";
    cout << "A proton has been constructed" << endl;
}

proton::~proton() {
    cout << "A proton will be destroyed" << endl;
}

void proton::print() {
    cout << "proton: " << "mass = " << mass << " spin = " << spin << " electric charge = " << electric_charge << " statistics = " << statistics << endl;
    up_quark1.print();
    up_quark2.print();
    down_quark1.print();
    gluon1.print();
    gluon2.print();
    gluon3.print();
}
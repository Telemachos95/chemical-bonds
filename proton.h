
#ifndef PROTON
#define PROTON

#include <string>
#include "up_quark.h"
#include "down_quark.h"
#include "gluon.h"
using namespace std;

class proton {
private:
    up_quark up_quark1;
    up_quark up_quark2;
    down_quark down_quark1;
    gluon gluon1;
    gluon gluon2;
    gluon gluon3;
    string mass;
    float spin;
    string electric_charge;
    string statistics;
public:
    proton();
    ~proton();
    void print();
};

#endif
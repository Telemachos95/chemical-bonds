
#include <iostream>
#include "up_quark.h"
using namespace std;

up_quark::up_quark() {
    electric_charge = "+2/3e";
    mass = "2.01 MeV/c*c";
    spin = 0.5;
    statistics = "Fermi-Dirac";
    cout << "A up quark has been constructed" << endl;
}

up_quark::~up_quark() {
    cout << "An up quark will be destroyed" << endl;
}

void up_quark::print() {
    cout << "up quark: " << "mass = " << mass << " spin = " << spin << " electric charge = " << electric_charge << " statistics = " << statistics << endl;
}

#ifndef UP_QUARK
#define UP_QUARK

#include <string>
using namespace std;

class up_quark {
private:
    string electric_charge;
    string mass;
    float spin;
    string statistics;
public:
    up_quark();
    ~up_quark();
    void print();
};

#endif